echo Deleting database
rm -R db.sqlite3

echo Loading migrations...
sudo docker-compose run web python manage.py makemigrations
sudo docker-compose run web python manage.py migrate

echo Loading fixtures...
for entry in `ls -v api/hotel_finder/fixtures`; do
     echo Load fixture: $entry
     docker-compose run web python3 manage.py loaddata $entry
 done
 echo The fixtures have been loaded successfully

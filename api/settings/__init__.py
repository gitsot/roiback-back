import environ
from .base import *  # noqa

env = environ.Env()
environ.Env.read_env()

if env('DJANGO_DEVELOPMENT') == "True":
    from .dev import *  # noqa
else:
    from .prod import *  # noqa

from django.db import models
from api.hotel_finder.models import Rate


class Inventory(models.Model):
    day = models.DateField()
    price = models.PositiveIntegerField()
    rate = models.ForeignKey(Rate, on_delete=models.CASCADE, related_name='inventories')

    class Meta:
        verbose_name_plural = "Inventories"
        db_table = "inventories"

    def __str__(self):
        return str(self.day)

from django.db import models
from api.hotel_finder.models import Hotel


class Room(models.Model):
    name = models.CharField(max_length=45)
    code = models.CharField(max_length=45)
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE, related_name='rooms')

    class Meta:
        verbose_name_plural = "Rooms"
        db_table = "rooms"

    def __str__(self):
        return self.name

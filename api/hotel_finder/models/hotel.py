from django.db import models


class Hotel(models.Model):
    name = models.CharField(max_length=45)
    code = models.CharField(max_length=45)

    class Meta:
        verbose_name_plural = "Hotels"
        db_table = "hotels"

    def __str__(self):
        return self.name

from django.db import models
from api.hotel_finder.models import Room


class Rate(models.Model):
    name = models.CharField(max_length=45)
    code = models.CharField(max_length=45)
    room = models.ForeignKey(Room, on_delete=models.CASCADE, related_name='rates')

    class Meta:
        verbose_name_plural = "Rates"
        db_table = "rates"

    def __str__(self):
        return self.name

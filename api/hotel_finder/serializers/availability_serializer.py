from rest_framework import serializers
from api.hotel_finder.models import Room, Rate
from django.db.models import Sum
from api.hotel_finder.serializers import InventorySerializerAllot


class RateAvailabilitySerializer(serializers.ModelSerializer):
    total_price = serializers.SerializerMethodField()
    breakdown = serializers.SerializerMethodField()

    def get_breakdown(self, obj):
        checkin_date = self.context.get("checkin_date")
        checkout_date = self.context.get("checkout_date")

        queryset = obj.inventories.filter(day__range=[checkin_date, checkout_date])
        serializer = InventorySerializerAllot(queryset, many=True)
        return serializer.data

    def get_total_price(self, obj):
        checkin_date = self.context.get("checkin_date")
        checkout_date = self.context.get("checkout_date")

        tp = obj.inventories.filter(rate_id=obj.id, day__range=[checkin_date, checkout_date]).aggregate(Sum('price'))
        return tp['price__sum']

    class Meta:
        model = Rate
        fields = ('code', 'total_price', 'breakdown')


class RoomAvailabilitySerializer(serializers.ModelSerializer):
    rates = RateAvailabilitySerializer(many=True, read_only=True)

    class Meta:
        model = Room
        fields = ('code', 'rates')

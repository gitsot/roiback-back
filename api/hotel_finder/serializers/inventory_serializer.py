from rest_framework import serializers
from api.hotel_finder.models import Inventory, Room


class InventorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventory
        fields = '__all__'


class InventorySerializerAllot(serializers.ModelSerializer):
    allotment = serializers.SerializerMethodField()

    def get_allotment(self, obj):
        day = obj.day
        allotment = Room.objects.filter(rates__inventories__day=day).count()
        return allotment

    class Meta:
        model = Inventory
        fields = ('day', 'price', 'allotment')

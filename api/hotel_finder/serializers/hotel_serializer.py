from rest_framework import serializers
from api.hotel_finder.models import Hotel


class HotelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hotel
        fields = '__all__'


class HotelSerializerRetrieve(serializers.ModelSerializer):
    class Meta:
        model = Hotel
        fields = ('code',)


class HotelSerializerList(serializers.ModelSerializer):
    class Meta:
        model = Hotel
        fields = ('code',)

    def to_representation(self, instance):
        return super().to_representation(instance)['code']

import json
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from api.hotel_finder.models import Hotel, Room, Rate
from api.hotel_finder.serializers import HotelSerializer, RoomSerializer, RateSerializer

c = APIClient()


class TestHotels(TestCase):
    def test_create_hotels(self):
        """
        This test function creates hotels, rooms, rates, and inventories to check its operation
        """
        print("TEST: Create three hotels")

        for i in range(3):
            data = {
                "name": "Hotel " + str(i),
                "code": "hotel_" + str(i),
            }
            response = c.post('/api/hotels/', data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = c.get('/api/hotels/', content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)

        self.assertEqual(len(result['hotels']), 3)
        self.assertEqual(result['hotels'][0], "hotel_0")
        self.assertEqual(result['hotels'][1], "hotel_1")
        self.assertEqual(result['hotels'][2], "hotel_2")

        print("TEST: Create three rooms per hotel")
        queryset = Hotel.objects.all()
        serializer = HotelSerializer(queryset, many=True)
        idx = 0
        for hotel in serializer.data:
            for i in range(3):
                id = hotel['id']
                data = {
                    "name": "Room " + str(idx),
                    "code": "room_" + str(idx),
                    "hotel": id,
                }
                response = c.post('/api/rooms/', data, format='json')
                self.assertEqual(response.status_code, status.HTTP_201_CREATED)
                idx += 1

        response = c.get('/api/rooms/', content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)

        self.assertEqual(len(result), 9)

        for i in range(len(result)):
            self.assertEqual(result[i]['code'], "room_" + str(i))

        print("TEST: Create two rates per room")
        queryset = Room.objects.all()
        serializer = RoomSerializer(queryset, many=True)
        idx = 0
        for room in serializer.data:
            for i in range(2):
                id = hotel['id']
                data = {
                    "name": "Rate " + str(idx),
                    "code": "rate_" + str(idx),
                    "room": id,
                }
                response = c.post('/api/rates/', data, format='json')
                self.assertEqual(response.status_code, status.HTTP_201_CREATED)
                idx += 1

        response = c.get('/api/rates/', content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)

        self.assertEqual(len(result), 18)

        for i in range(len(result)):
            self.assertEqual(result[i]['code'], "rate_" + str(i))

        print("TEST: Create two inventories per rate")
        queryset = Rate.objects.all()
        serializer = RateSerializer(queryset, many=True)
        idx = 0
        for rate in serializer.data:
            for i in range(2):
                id = hotel['id']
                data = {
                    "day": "2022-01-0" + str(i + 1),
                    "price": 100 * idx,
                    "rate": id,
                }
                response = c.post('/api/inventories/', data, format='json')
                self.assertEqual(response.status_code, status.HTTP_201_CREATED)

                idx += 1

        response = c.get('/api/inventories/', content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)

        self.assertEqual(len(result), 36)

        print("TEST: Test availability endpoint")
        response = c.get('/api/availability/hotel_0/2022-01-01/2023-01-18/', content_type='application/json')
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

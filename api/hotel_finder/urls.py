from django.urls import include, path
from rest_framework import routers
from api.hotel_finder import views

router = routers.DefaultRouter()
router.register('hotels', views.HotelViewSet, basename="hotels")
router.register('rooms', views.RoomViewSet, basename="rooms")
router.register('rates', views.RateViewSet, basename="rates")
router.register('inventories', views.InventoryViewSet, basename="inventory")

urlpatterns = [
    path('', include(router.urls)),
    path('availability/<str:code>/<str:checkin_date>/<str:checkout_date>/', views.Availability.as_view(), name='availability'),
]

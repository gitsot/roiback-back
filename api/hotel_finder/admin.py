from api.hotel_finder import models
from django.contrib import admin


@admin.register(models.Hotel)
class HotelAdmin(admin.ModelAdmin):
    list_display = ('name', 'code')
    search_fields = ('name', 'code')


@admin.register(models.Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'hotel')
    search_fields = ('name', 'code', 'hotel')


@admin.register(models.Rate)
class RateAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'room')
    search_fields = ('name', 'code', 'room')


@admin.register(models.Inventory)
class InventoryAdmin(admin.ModelAdmin):
    list_display = ('day', 'price', 'rate')
    search_fields = ('day', 'price', 'rate')

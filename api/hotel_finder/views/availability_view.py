from rest_framework.views import APIView
from rest_framework.response import Response
from api.hotel_finder.models import Room
from api.hotel_finder.serializers import RoomAvailabilitySerializer


class Availability(APIView):
    def get(self, request, code, checkin_date, checkout_date):
        """
        This function returns the availabilities of the hotels with a breakdown of rates
        """
        queryset = Room.objects.prefetch_related('rates', 'rates__inventories').filter(hotel__code=code, rates__inventories__day__range=[checkin_date, checkout_date]).distinct()
        serializer = RoomAvailabilitySerializer(queryset, many=True, context={'checkin_date': checkin_date, 'checkout_date': checkout_date})
        return Response(serializer.data)

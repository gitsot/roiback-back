from rest_framework import viewsets
from api.hotel_finder.models import Room
from api.hotel_finder.serializers import RoomSerializer


class RoomViewSet(viewsets.ModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    filterset_fields = '__all__'

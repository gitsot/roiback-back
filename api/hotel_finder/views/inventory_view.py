from rest_framework import viewsets
from api.hotel_finder.models import Inventory
from api.hotel_finder.serializers import InventorySerializer, InventorySerializerAllot


class InventoryViewSet(viewsets.ModelViewSet):
    queryset = Inventory.objects.all()
    serializer_class = InventorySerializerAllot
    filterset_fields = '__all__'

    def get_serializer_class(self):
        if self.action == 'list':
            return InventorySerializerAllot
        if self.action == 'retrieve':
            return InventorySerializer

        return InventorySerializer

from rest_framework import viewsets
from api.hotel_finder.models import Hotel
from api.hotel_finder.serializers import HotelSerializerList, HotelSerializer, HotelSerializerRetrieve
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page


class HotelViewSet(viewsets.ModelViewSet):
    queryset = Hotel.objects.all()
    serializer_class = HotelSerializerList
    filterset_fields = '__all__'
    lookup_field = 'code'

    def get_serializer_class(self):
        if self.action == 'list':
            return HotelSerializerList
        if self.action == 'retrieve':
            return HotelSerializerRetrieve

        return HotelSerializer

    @method_decorator(cache_page(60 * 60 * 2))
    def list(self, request, *args, **kwargs):
        response = super(HotelViewSet, self).list(request, *args, **kwargs)
        response.data = {"hotels": response.data}
        return response

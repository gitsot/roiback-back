from rest_framework import viewsets
from api.hotel_finder.models import Rate
from api.hotel_finder.serializers import RateSerializer


class RateViewSet(viewsets.ModelViewSet):
    queryset = Rate.objects.all()
    serializer_class = RateSerializer
    filterset_fields = '__all__'

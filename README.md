### Introducción

Este proyecto forma parte de la prueba Full-Stack Developer de Roiback, el objetivo es crear un pequeño sistema de búsqueda de habitaciones de hotel. La API la está hecha con Django rest framework, y la parte front con React.

La parte de React, ha sido completamente nueva para mi, he tenido que dedicar un tiempo a aprendizaje ya que nunca había hecho ningún proyecto en este framework, y me ha resultado un framework muy sencillo de utilizar.

### Esquema ER

![](../../../Imágenes/ER.png)

### Elección de la base de datos

La base de datos elegida ha sido sqlite, para proyectos personales, mi preferencia siempre ha sido postgresql, ya que es una base de datos compleja y muy completa que da todo tipo de posiblidades, pero para un proyecto pequeño, en mi opinión, la mejor opción es uitilizar una base de datos sencilla y rápida de utilizar.

### Puesta en marcha

Para poner en marcha este proyecto es necesario tener instalado:

* docker-compose
* node.js

1. Crear el archivo .env dentro de `api/settings/` con la siguiente línea:
   
    `DJANGO_DEVELOPMENT=True`


2. Ejecutar comando de up y build de docker. Ejecutar en raíz.
   
    `sudo docker-compose up --build`


3. Cargar fixtures para tener datos predefinidos, esto ejecuta las migraciones también. Ejecutar en raíz y con sudo.

    `sudo sh api/system/load_fixtures.sh`


4. Ejecutar el front, desde raíz.

    `npm start`

### Swagger

Este proyecto tiene un swagger en el que se muestran todos los endpoints, para acceder a el, hay que ir a la siguiente ruta:
    `http://localhost:8000/swagger-docs/`

![img.png](img.png)

### Admin
Para poder gestionar la API con mejor facilidad, se puede acceder al panel de administrador desde la siguiente ruta :
    `http://localhost:8000/admin/`

Con los fixtures, se ha introducido un usuario administrador con el que se puede iniciar sesión.
    
    Username: admin
    Password: admin

![img_1.png](img_1.png)

### Test unitarios

Para poner en marcha los test unitarios hay que ejecutar el comando:

`sudo docker-compose run web python3 manage.py test`

### flake8

Se ha utilizado el estandar PEP-8 para el estructura del código, y para su corrección se ha utilizado el siguiente comando desde `api/`

`flake8 --ignore=E501`


### Front

Para acceder al front hay que ir a la ruta `http://localhost:3000/` y la siguiente imagen es un ejemplo de búsquedas de hoteles.

![img_2.png](img_2.png)


### Mejoras pendientes

Este proyecto lo he hecho en ratos libres, me hubiera gustado terminarlo al 100% y darle toques personales que no me han dado tiempo, pero aquí dejo una lista de los futuros cambios que haría.

* Estilos en el front, el dropdown, los datapicker y el botón, están sin estilos. Y también añadiría otros estilos como un hover que al pasar el ratón por las tarifas, se oscurezca el fondo.
* Texto resumen de búsquedas, debajo del formulario, al hacer una búsqueda, debería salir un texto con la cantidad de noches seleccionadas.
* Pipelines, me parece importante la integración de pipelines con gitlab, es un punto que me hubiera gustado implementar.
* Más test unitarios.
* Posibilidad de reservar habitación.
* Implementar un login con auth-jwt restringiendo ciertos endpoints solo para admins.
* Una documentación extensa con postman.